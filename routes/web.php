<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view('authentication.login');
});

Route::get('users', function () {
    // Myrtle\Users\User::where
});

Route::post('/genders/seed', 'Genders\Seed')->name('genders.seed');

Route::post('/maritals/seed', 'Maritals\Seed')->name('maritals.seed');
