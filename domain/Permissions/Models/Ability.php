<?php

namespace Myrtle\omain\Permissions\Models;

use Myrtle\Users\User;

class Ability
{
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    protected $timestamps = false;

    /**
     * Get the Users of the Ability
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->morphToMany(User::class, 'permissionable', 'permissions', 'ability_id', 'permissionable_id');
    }
}
