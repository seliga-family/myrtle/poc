<?php

namespace Myrtle\Emails;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address',
    ];

    /**
     * Get owning Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function emailable()
    {
        return $this->morphTo();
    }
}
