<?php

namespace Myrtle\Emails\Concerns;

use Myrtle\Emails\Email;

trait Emailable
{
    /**
     * Related emails for this Model.
     *
     * @return mixed
     */
    public function emails()
    {
        return $this->morphMany(Email::class, 'emailable');
    }
}
