<?php

namespace Myrtle\People\Concerns;

use Myrtle\People\Biograph;
use Myrtle\People\Demographics\Gender;
use Myrtle\People\Demographics\Marital;
use Myrtle\People\Demographics\Religion;
use Illuminate\Database\Eloquent\Builder;
use Myrtle\People\Demographics\Ethnicity;

trait HasBiograph
{
    /**
     * Get the Biograph for the Person.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function biograph()
    {
        return $this->morphOne(Biograph::class, 'personable')->withDefault();
    }

    /**
     * Scope Biographs by Gender.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Myrtle\People\Demographics\Gender $gender
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByGender(Builder $query, Gender $gender)
    {
        return $query->whereHas('biograph', function ($q) use ($gender) {
            $q->where('gender_id', $gender->id);
        });
    }

    /**
     * Scope Biographs by MaritalStatus.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Myrtle\People\Demographics\Marital $marital
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByMarital(Builder $query, Marital $marital)
    {
        return $query->whereHas('biograph', function ($query) use ($marital) {
            $query->where('marital_id', $marital->getKey());
        });
    }

    /**
     * Scope Biographs by Religion.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \App\Models\Religion $religion
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByReligion(Builder $query, Religion $religion)
    {
        return $query->whereHas('biograph', function ($q) use ($religion) {
            $q->where('religion_id', $religion->id);
        });
    }

    /**
     * Scope Biographs by Ethnicity.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \App\Models\Ethnicity $ethnicity
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByEthnicity(Builder $query, Ethnicity $ethnicity)
    {
        return $query->whereHas('biograph', function ($q) use ($ethnicity) {
            $q->where('ethnicity_id', $ethnicity->id);
        });
    }
}
