<?php

namespace Myrtle\People\Concerns;

trait IsPerson
{
    use HasName, HasBiograph;
}
