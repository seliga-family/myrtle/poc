<?php

namespace Myrtle\People\Concerns\Name;

use Illuminate\Support\Str;

trait Presenter
{
    /**
     * Get preferred.
     *
     * @param mixed $value
     *
     * @return void
     */
    public function getPreferredAttribute($value)
    {
        return ! ! $value ? $value : $this->first;
    }

    /**
     * Get full name.
     *
     * @return string
     */
    public function getFullAttribute()
    {
        return collect([$this->first, $this->middle, $this->last])->filter->implode(' ');
    }

    /**
     * Get first and last name.
     *
     * @return string
     */
    public function getFirstLastAttribute()
    {
        return $this->first . ' ' . $this->last;
    }

    /**
     * Get preferred and last name.
     *
     * @return string
     */
    public function getPreferredLastAttribute()
    {
        return $this->preferred . ' ' . $this->last;
    }

    /**
     * Get last, first name.
     *
     * @return string
     */
    public function getLastFirstAttribute()
    {
        return $this->last . ', ' . $this->first;
    }

    /**
     * Get first initial.
     *
     * @return string
     */
    public function getFirstInitialAttribute()
    {
        return Str::substr($this->first, 0, 1);
    }

    /**
     * Get middle initial.
     *
     * @return string
     */
    public function getMiddleInitialAttribute()
    {
        return ! ! $this->middle ? Str::substr($this->middle, 0, 1) : null;
    }

    /**
     * Get last initial.
     *
     * @return string
     */
    public function getLastInitialAttribute()
    {
        return Str::substr($this->last, 0, 1);
    }

    /**
     * Get initials.
     *
     * @return string
     */
    public function getInitialsAttribute()
    {
        return $this->first_initial . $this->middle_initial . $this->last_initial;
    }

    /**
     * Get first and last initials.
     *
     * @return string
     */
    public function getFirstLastInitialsAttribute()
    {
        return $this->first_initial . $this->last_initial;
    }

    /**
     * Get last and first initials.
     *
     * @return string
     */
    public function getLastFirstInitialsAttribute()
    {
        return $this->last_initial . $this->first_initial;
    }

    /**
     * Set preferred.
     *
     * @param  string  $value
     *
     * @return void
     */
    public function setPreferredAttribute($value)
    {
        $this->attributes['preferred'] = ($value !== $this->first) ? $value : null;
    }
}
