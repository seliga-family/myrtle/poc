<?php

namespace Myrtle\People\Concerns;

use Myrtle\People\Name;

trait HasName
{
    /**
     * Get the Name for the Person.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function name()
    {
        return $this->morphOne(Name::class, 'personable')->withDefault();
    }
}
