<?php

namespace Myrtle\People;

use Illuminate\Database\Eloquent\Model;
use Myrtle\People\Events\Name as Events;
use  Myrtle\People\Concerns\Name\Presenter;
use Illuminate\Database\Eloquent\SoftDeletes;

class Name extends Model
{
    use Presenter, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'person_names';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last',
        'first',
        'middle',
        'monikers',
        'preferred',
    ];

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'deleted_at';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        self::DELETED_AT,
        Model::CREATED_AT,
        Model::UPDATED_AT,
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => Events\Saved::class,
        'saving' => Events\Saving::class,
        'created' => Events\Created::class,
        'deleted' => Events\Deleted::class,
        'updated' => Events\Updated::class,
        'creating' => Events\Creating::class,
        'deleting' => Events\Deleting::class,
        'restored' => Events\Restored::class,
        'updating' => Events\Updating::class,
        'restoring' => Events\Restoring::class,
    ];

    /**
     * The relationships that should be touched on save.
     *
     * @var array
     */
    protected $touches = [
        'personable',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'monikers' => 'json',
    ];

    /**
     * Get owning personable Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function personable()
    {
        return $this->morphTo();
    }
}
