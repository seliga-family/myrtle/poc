<?php

namespace Myrtle\People\Demographics\Concerns;

use Myrtle\People\Demographics\Religion;

trait BelongsToReligion
{
    /**
     * Get the Religion.
     *
     * @return void
     */
    public function religion()
    {
        return $this->belongsTo(Religion::class)->withDefault();
    }

    /**
     * Pass-through religion_id setter.
     *
     * @param \Myrtle\People\Demographics\Religion $religion
     *
     * @return void
     */
    public function setReligionAttribute(Religion $religion)
    {
        $this->attributes['religion_id'] = $religion->getKey();

        $this->setRelation('religion', $religion);
    }
}
