<?php

namespace Myrtle\People\Demographics\Concerns;

use Myrtle\People\Demographics\Ethnicity;

trait BelongsToEthnicity
{
    /**
     * Get the Ethnicity.
     *
     * @return mixed
     */
    public function ethnicity()
    {
        return $this->belongsTo(Ethnicity::class)->withDefault();
    }

    /**
     * Pass-through ethnicity_id setter.
     *
     * @param \Myrtle\People\Demographics\Ethnicity $ethnicity
     *
     * @return void
     */
    public function setEthnicityAttribute(Ethnicity $ethnicity)
    {
        $this->attributes['ethnicity_id'] = $ethnicity->getKey();

        $this->setRelation('ethnicity', $ethnicity);
    }
}
