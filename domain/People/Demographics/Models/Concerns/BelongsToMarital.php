<?php

namespace Myrtle\People\Demographics\Concerns;

use Myrtle\People\Demographics\Marital;

trait BelongsToMarital
{
    /**
     * Get the Marital
     *
     * @return mixed
     */
    public function marital()
    {
        return $this->belongsTo(Marital::class)->withDefault();
    }

    /**
     * Pass-through marital_id setter.
     *
     * @param \Myrtle\People\Demographics\Marital $marital
     *
     * @return void
     */
    public function setMaritalAttribute(Marital $marital)
    {
        $this->attributes['marital_id'] = $marital->getKey();

        $this->setRelation('marital', $marital);
    }
}
