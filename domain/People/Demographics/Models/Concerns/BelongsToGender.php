<?php

namespace Myrtle\People\Demographics\Concerns;

use Myrtle\People\Demographics\Gender;

trait BelongsToGender
{
    /**
     * Get the Gender.
     *
     * @return mixed
     */
    public function gender()
    {
        return $this->belongsTo(Gender::class)->withDefault();
    }

    /**
     * Pass-through gender_id setter.
     *
     * @param \Myrtle\People\Demographics\Gender $gender
     *
     * @return void
     */
    public function setGenderAttribute(Gender $gender)
    {
        $this->attributes['gender_id'] = $gender->getKey();

        $this->setRelation('gender', $gender);
    }
}
