<?php

namespace Myrtle\People;

use Illuminate\Database\Eloquent\Model;
use Myrtle\People\Events\Biograph as Events;
use Illuminate\Database\Eloquent\SoftDeletes;
use Myrtle\People\Demographics\Concerns\BelongsToGender;
use Myrtle\People\Demographics\Concerns\BelongsToMarital;
use Myrtle\People\Demographics\Concerns\BelongsToReligion;
use Myrtle\People\Demographics\Concerns\BelongsToEthnicity;

class Biograph extends Model
{
    use SoftDeletes,
        BelongsToGender,
        BelongsToReligion,
        BelongsToEthnicity,
        BelongsToMarital;

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'deleted_at';

    /**
     * The name of the "birth date" column.
     *
     * @var string
     */
    const BIRTH_DATE = 'birth_date';

    /**
     * The name of the "death date" column.
     *
     * @var string
     */
    const DEATH_DATE = 'death_date';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'person_biographs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'birth_date',
        'gender_id',
        'ethnicity_id',
        'marital_id',
        'religion_id',
        'death_date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        self::BIRTH_DATE,
        self::DEATH_DATE,
        Model::CREATED_AT,
        Model::UPDATED_AT,
        self::DELETED_AT,
    ];

    protected $with = [
        'gender',
        'marital',
        'religion',
        'ethnicity',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => Events\Saved::class,
        'saving' => Events\Saving::class,
        'created' => Events\Created::class,
        'deleted' => Events\Deleted::class,
        'updated' => Events\Updated::class,
        'creating' => Events\Creating::class,
        'deleting' => Events\Deleting::class,
        'restored' => Events\Restored::class,
        'updating' => Events\Updating::class,
        'restoring' => Events\Restoring::class,
        // 'retrieved' => Events\Retrieved::class,
    ];

    /**
     * The relationships that should be touched on save.
     *
     * @var array
     */
    protected $touches = [
        'personable',
    ];

    /**
     * Get owning personable Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function personable()
    {
        return $this->morphTo();
    }
}
