<?php

namespace Myrtle\People\Events\Name;

use Myrtle\People\Name;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Saving implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets;

    public $name;

    /**
     * Create a new event instance.
     *
     * @param Name $name
     */
    public function __construct(Name $name)
    {
        $this->name = $name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
//        return new PrivateChannel('personable.' . '.' . $this->name->personable_type . '.' . $this->name->personable->id . '.name');
    }
}
