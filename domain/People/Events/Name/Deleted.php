<?php

namespace Myrtle\People\Events\Name;

use Myrtle\People\Name;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class Deleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $name;

    /**
     * Create a new event instance.
     *
     * @param Name $name
     */
    public function __construct(Name $name)
    {
        $this->name = $name;
    }
}
