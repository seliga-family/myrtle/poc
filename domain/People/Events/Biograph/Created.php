<?php

namespace Myrtle\People\Events\Biograph;

use Myrtle\People\Biograph;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Created implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $biograph;

    /**
     * Create a new event instance.
     *
     * @param Biograph $biograph
     */
    public function __construct(Biograph $biograph)
    {
        $this->biograph = $biograph;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('personable.' . '.' . $this->biograph->personable_type . '.' . $this->biograph->personable->id . '.biograph');
    }
}
