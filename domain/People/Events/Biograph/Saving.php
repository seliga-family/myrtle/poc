<?php

namespace Myrtle\People\Events\Biograph;

use Myrtle\People\Biograph;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class Saving
{
    use Dispatchable, InteractsWithSockets;

    public $biograph;

    /**
     * Create a new event instance.
     *
     * @param Biograph $biograph
     */
    public function __construct(Biograph $biograph)
    {
        $this->biograph = $biograph;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        User::create([
            'email' => 'some@email.com',
            'name.first' => 'Justin',
            'name.last' => 'Seliga',
        ]);

        return new PrivateChannel('personable.' . '.' . $this->biograph->personable_type . '.' . $this->biograph->personable->id . '.biograph');
    }
}
