<?php

namespace Myrtle\People\Events\Biograph;

use Myrtle\People\Biograph;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class Deleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $biograph;

    /**
     * Create a new event instance.
     *
     * @param Biograph $biograph
     */
    public function __construct(Biograph $biograph)
    {
        $this->biograph = $biograph;
    }
}
