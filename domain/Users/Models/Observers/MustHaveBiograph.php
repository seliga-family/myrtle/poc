<?php

namespace Myrtle\Users\Models\Observers;

use Myrtle\Users\User;

class MustHaveBiograph
{
    public function created(User $user)
    {
        $user->biograph()->create($user->biograph->toArray());
    }
}
