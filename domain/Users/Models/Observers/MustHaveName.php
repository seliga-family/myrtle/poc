<?php

namespace Myrtle\Users\Models\Observers;

use Myrtle\Users\User;

class MustHaveName
{
    public function created(User $user)
    {
        $user->name()->create($user->name->toArray());
    }
}
