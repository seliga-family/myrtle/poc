<?php

namespace Myrtle\Users\Models\Concerns;

use Myrtle\Users\Models\Observers;

trait MustHaveName
{
    public static function bootMustHaveName()
    {
        static::observe(Observers\MustHaveName::class);
    }
}
