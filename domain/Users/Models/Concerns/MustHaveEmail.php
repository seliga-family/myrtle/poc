<?php

namespace Myrtle\Users\Models\Concerns;

use Myrtle\Users\Models\Observers;

trait MustHaveEmail
{
    public static function bootMustHaveEmail()
    {
        static::observe(Observers\MustHaveEmail::class);
    }
}
