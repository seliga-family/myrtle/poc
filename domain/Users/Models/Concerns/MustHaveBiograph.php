<?php

namespace Myrtle\Users\Models\Concerns;

use Myrtle\Users\Models\Observers;

trait MustHaveBiograph
{
    public static function bootMustHaveBiograph()
    {
        static::observe(Observers\MustHaveBiograph::class);
    }
}
