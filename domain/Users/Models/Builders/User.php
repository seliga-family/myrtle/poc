<?php

namespace Myrtle\Users\Builders;

use Illuminate\Database\Eloquent\Builder;

class User extends Builder
{
    public function visitedThisWeek()
    {
        return $this->whereNotNull('last_visit_at');
    }
}
