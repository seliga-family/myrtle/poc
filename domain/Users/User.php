<?php

namespace Myrtle\Users;

use App\User as LaravelUser;
use Myrtle\Users\Models\Concerns;
use Myrtle\People\Concerns\IsPerson;
use Myrtle\Emails\Concerns\Emailable;
use Illuminate\Database\Eloquent\Model;
use Myrtle\Users\Builders\User as Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Myrtle\People\Demographics\Concerns\BelongsToGender;
use Myrtle\People\Demographics\Concerns\BelongsToMarital;
use Myrtle\People\Demographics\Concerns\BelongsToReligion;
use Myrtle\People\Demographics\Concerns\BelongsToEthnicity;

class User extends LaravelUser
{
    use BelongsToEthnicity;
    use BelongsToGender;
    use BelongsToMarital;
    use BelongsToReligion;
    use Concerns\MustHaveName;
    use Concerns\MustHaveEmail;
    use Concerns\MustHaveBiograph;
    use Emailable;
    use IsPerson;
    use SoftDeletes;

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'deleted_at';

    /**
     * The name of the "disabled at" column.
     *
     * @var string
     */
    const DISABLED_AT = 'disabled_at';

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'timezone' => 'UTC',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        Model::CREATED_AT,
        Model::UPDATED_AT,
        self::DISABLED_AT,
        self::DELETED_AT,
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => Events\Saved::class,
        'saving' => Events\Saving::class,
        'created' => Events\Created::class,
        'deleted' => Events\Deleted::class,
        'updated' => Events\Updated::class,
        'creating' => Events\Creating::class,
        'deleting' => Events\Deleting::class,
        'restored' => Events\Restored::class,
        'updating' => Events\Updating::class,
        'restoring' => Events\Restored::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password',
        'timezone',
        self::DISABLED_AT,
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $with = [
        'name',
        'biograph',
    ];

    /**
     * Get timezone.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public function getTimezoneAttribute($value)
    {
        /** @var Model $value */
        $test = $value;

        return $value;
    }

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     *
     * @return \Myrtle\Users\Users
     */
    public function newCollection(array $models = [])
    {
        return new Users($models);
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     *
     * @return \Myrtle\Users\Builders\User|static
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }
}
