<?php

namespace Myrtle\Users\Jobs;

use Myrtle\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $name;

    protected $emails;

    protected $password;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param string $address
     *
     * @return void
     */
    public function __construct(string $password, array $name, array $emails)
    {
        $this->password = $password;
        $this->name = collect($name);
        $this->emails = collect($emails);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::create(['password' => $this->password]);

        UpdateName::dispatch($user, $this->name->toArray());

        $this->emails->each(function ($address) use ($user) {
            CreateEmail::dispatch($user, $address);
        });

        return $user;
    }
}
