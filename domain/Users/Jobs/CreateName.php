<?php

namespace Myrtle\Users\Jobs;

use Myrtle\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateName implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $name;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param array $name
     *
     * @return void
     */
    public function __construct(User $user, array $name)
    {
        $this->user = $user;
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->name()->create($this->name);
    }
}
