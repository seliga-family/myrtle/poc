<?php

namespace Myrtle\Users\Jobs;

use Myrtle\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The user.
     *
     * @var User
     */
    protected $user;

    /**
     * The email address.
     *
     * @var string
     */
    protected $address;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param string $address
     *
     * @return void
     */
    public function __construct(User $user, string $address)
    {
        $this->user = $user;
        $this->address = $address;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->emails()->create(['address' => $this->address]);
    }
}
