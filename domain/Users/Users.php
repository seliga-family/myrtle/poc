<?php

namespace Myrtle\Users;

use Illuminate\Database\Eloquent\Collection;

class Users extends Collection
{
    public function onCollection()
    {
        return $this;
    }
}
