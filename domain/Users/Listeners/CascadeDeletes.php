<?php

namespace Myrtle\Users\Listeners;

use Myrtle\Users\Events\Deleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CascadeDeletes implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Handle the event.
     *
     * @param Deleted $event
     * @return void
     */
    public function handle(Deleted $event)
    {
        // TODO: Since this is queued we need to make sure the user hasn't been restored first.
        // Does it make sense to instead have jobs queued that Delete / Force Delete these relationships?
        // Each job can then make necessary checks such as this to ensure integrity?
        $event->user->name->delete();
        $event->user->biograph->delete();
    }
}
