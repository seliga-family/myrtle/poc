<?php

namespace Myrtle\Users\Validation\Rules;

use Illuminate\Validation\Rule;

class UniqueEmail extends Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->morphTo();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The email address :attribute is already taken.';
    }
}
