<?php

namespace Tests\Feature\Http\Controllers\Maritals;

use Tests\TestCase;
use Myrtle\Users\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Myrtle\People\Demographics\Marital;

class SeedTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Ensure MaritalStatus can be seeded from the frontend.
     *
     * @return void
     */
    public function testCanSeedData()
    {
        $user = factory(User::class)->create();

        $this->be($user);
        $this->post(route('maritals.seed'));

        $this->assertSame(collect(\MaritalsSeeder::$names)->count(), Marital::count());
    }

    /**
     * Ensure the Seeder is indempotent.
     *
     * @return void
     */
    public function testSeederIsIdempotent()
    {
        $user = factory(User::class)->create();

        $this->be($user);
        $this->post(route('maritals.seed'));
        $this->post(route('maritals.seed'));

        $this->assertCount(collect(\MaritalsSeeder::$names)->count(), Marital::all());
    }
}
