<?php

namespace Tests\Feature\Http\Controllers\Genders;

use GendersSeeder;
use Tests\TestCase;
use Myrtle\Users\User;
use Myrtle\People\Demographics\Gender;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SeedTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Ensure Gender can be seeded from the frontend.
     *
     * @return void
     */
    public function testCanSeedData()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->be($user);
        $this->post(route('genders.seed'));

        $this->assertSame(collect(GendersSeeder::$names)->count(), Gender::count());
    }

    /**
     * Ensure the Seeder is indempotent.
     *
     * @return void
     */
    public function testSeederIsIdempotent()
    {
        $user = factory(User::class)->create();

        $this->be($user);
        $this->post(route('genders.seed'));
        $this->post(route('genders.seed'));

        $this->assertCount(collect(GendersSeeder::$names)->count(), Gender::all());
    }
}
