<?php

namespace Tests\Feature\Models\User;

use Tests\TestCase;
use Myrtle\Users\User;
use Myrtle\People\Name;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NameTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Ensure the User always has Name returned.
     *
     * @return void
     */
    public function testNameRelationshipReturnsDefault()
    {
        $user = factory(User::class)->create();

        $this->assertInstanceOf(Name::class, $user->name);
    }

    /**
     * Ensure the user has single name record.
     *
     * @return void
     */
    public function testNameFillableBeforePersisting()
    {
        $user = factory(User::class)->make();
        $name = factory(Name::class)->make();

        $user->name->fill($name->toArray());

        $this->assertInstanceOf(Name::class, $user->name);

        $this->assertEquals($name->first, $user->name->first);
        $this->assertEquals($name->middle, $user->name->middle);
        $this->assertEquals($name->last, $user->name->last);
    }

    /**
     * Ensure the Name is persisted automatically when the User is created.
     *
     * @return void
     */
    public function testNamePersistedWhenUserCreated()
    {
        $user = factory(User::class)->create();

        $this->assertInstanceOf(Name::class, $user->fresh()->name);
    }

    /**
     * Ensure the Name is fillable before automatically being persisted on User create.
     *
     * @return void
     */
    public function testNamePersistedAfterFilledAndUserCreated()
    {
        $user = factory(User::class)->make();
        $name = factory(Name::class)->make();

        $user->name->fill($name->toArray());
        $user->save();
        $fresh = $user->fresh();

        $this->assertInstanceOf(Name::class, $user->name);

        $this->assertEquals($name->first, $fresh->name->first);
        $this->assertEquals($name->middle, $fresh->name->middle);
        $this->assertEquals($name->last, $fresh->name->last);
    }
}
