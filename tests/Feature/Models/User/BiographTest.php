<?php

namespace Tests\Feature\Models\User;

use Tests\TestCase;
use Myrtle\Users\User;
use Myrtle\People\Biograph;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BiographTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Ensure the User always has biograph returned.
     */
    public function testBiographRelationshipReturnsDefault()
    {
        $user = factory(User::class)->create();

        $this->assertInstanceOf(Biograph::class, $user->biograph);
    }

    /**
     * Ensure the user has single biograph record.
     */
    public function testBiographFillableBeforePersisting()
    {
        $user = factory(User::class)->make();
        $biograph = factory(Biograph::class)->make();

        $user->biograph->fill($biograph->toArray());

        $this->assertInstanceOf(Biograph::class, $user->biograph);

        $this->assertEquals($biograph->gender->name, $user->biograph->gender->name);
        $this->assertEquals($biograph->religion->name, $user->biograph->religion->name);
        $this->assertEquals($biograph->ethnicity->name, $user->biograph->ethnicity->name);
        $this->assertEquals($biograph->marital->name, $user->biograph->marital->name);
    }

    /**
     * Ensure the biograph is persisted automatically when the User is created.
     */
    public function testBiographPersistedWhenUserCreated()
    {
        $user = factory(User::class)->create();

        $this->assertInstanceOf(Biograph::class, $user->fresh()->biograph);
    }

    /**
     * Ensure the biograph is fillable before automatically being persisted on User create email.
     */
    public function testBiographPersistedAfterFilledAndUserCreated()
    {
        $user = factory(User::class)->make();
        $biograph = factory(Biograph::class)->make();

        $user->biograph->fill($biograph->toArray());
        $user->save();
        $fresh = $user->fresh();

        $this->assertInstanceOf(Biograph::class, $user->biograph);

        $this->assertEquals($biograph->gender->name, $fresh->biograph->gender->name);
        $this->assertEquals($biograph->religion->name, $fresh->biograph->religion->name);
        $this->assertEquals($biograph->ethnicity->name, $fresh->biograph->ethnicity->name);
        $this->assertEquals($biograph->marital->name, $fresh->biograph->marital->name);
    }
}
