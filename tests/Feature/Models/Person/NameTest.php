<?php

namespace Tests\Feature\Models\Person;

use Tests\TestCase;
use Myrtle\People\Name;

class NameTest extends TestCase
{
    protected $name = [
        'first' => 'Justin',
        'middle' => 'Ross',
        'last' => 'Seliga',
        'preferred' => '',
        'monikers' => [],
    ];

    /**
     * Test that preferred operates as a traditional column.
     *
     * @return void
     */
    public function test_prefered_is_used()
    {
        $name = factory(Name::class)->make($this->name)->fill(['preferred' => 'agiles']);

        $this->assertEquals('agiles', $name->preferred);
    }

    /**
     * Test that preferred is not set if value is same as first.
     *
     * @return void
     */
    public function test_preferred_cannot_be_same_as_first()
    {
        $name = factory(Name::class)->make($this->name)->fill(['preferred' => array_get($this->name, 'first')]);

        $this->assertNull(array_get($name->getDirty(), 'preferred'));
    }

    /**
     * Test first is returned if preferred is empty.
     *
     * @return void
     */
    public function test_first_is_used_if_preferred_is_null()
    {
        $name = factory(Name::class)->make($this->name);

        $this->assertEquals('Justin', $name->preferred);
    }

    /**
     * Test last, first name returned.
     *
     * @return void
     */
    public function test_can_retrieve_first_comma_last()
    {
        $name = factory(Name::class)->make($this->name);

        $this->assertEquals('Seliga, Justin', $name->last_first);
    }

    /**
     * Test first initial is retruned.
     *
     * @return void
     */
    public function test_can_retrieve_first_initial()
    {
        $name = factory(Name::class)->make($this->name);

        $this->assertEquals('J', $name->first_initial);
    }

    /**
     * Test middle initial is returend.
     *
     * @return void
     */
    public function test_can_retrieve_middle_initial()
    {
        $name = factory(Name::class)->make($this->name);

        $this->assertEquals('R', $name->middle_initial);
    }

    /**
     * Test last initial is returend.
     *
     * @return void
     */
    public function test_can_retrieve_last_initial()
    {
        $name = factory(Name::class)->make($this->name);

        $this->assertEquals('S', $name->last_initial);
    }

    /**
     * Test full initial is returend.
     *
     * @return void
     */
    public function test_can_retrieve_full_initials()
    {
        $name = factory(Name::class)->make($this->name);

        $this->assertEquals('JRS', $name->initials);
    }

    /**
     * Test full initial is returend when name does not include middle.
     *
     * @return void
     */
    public function test_can_retrieve_full_initials_with_empty_middle()
    {
        $name = factory(Name::class)->make([
            'first' => 'Justin',
            'middle' => null,
            'last' => 'Seliga',
        ]);

        $this->assertEquals('JS', $name->initials);
    }

    /**
     * Test first and last initials are returend.
     *
     * @return void
     */
    public function test_can_retrieve_first_last_initials()
    {
        $name = factory(Name::class)->make($this->name);

        $this->assertEquals('JS', $name->first_last_initials);
    }

    /**
     * Test first and last initials are returend.
     *
     * @return void
     */
    public function test_can_retrieve_last_first_initials()
    {
        $name = factory(Name::class)->make($this->name);

        $this->assertEquals('SJ', $name->last_first_initials);
    }

    /**
     * Ensure Name cannot be soft deleted unless Personable has already been soft deleted.
     *
     * @return void
     */
    public function test_cannot_be_hard_deleted_if_personable_is_not_soft_deleted()
    {
        // $this->markTestSkipped('TODO: Test name cannot be soft deleted if Personable is not soft deleted');
    }
}
