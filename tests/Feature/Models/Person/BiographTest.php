<?php

namespace Tests\Feature\Models\Person;

use Tests\TestCase;
use Myrtle\People\Biograph;
use Myrtle\People\Demographics\Gender;
use Myrtle\People\Demographics\Marital;
use Myrtle\People\Demographics\Religion;
use Myrtle\People\Demographics\Ethnicity;

class BiographTest extends TestCase
{
    public function testReligionCanBeSetFromModel()
    {
        $religion = factory(Religion::class)->make();

        $biograph = factory(Biograph::class)->make()->fill([
            'religion' => $religion,
        ]);

        $this->assertInstanceOf(Religion::class, $biograph->religion);
    }

    public function testEthnicityCanBeSetFromModel()
    {
        $ethnicity = factory(Ethnicity::class)->make();

        $biograph = factory(Biograph::class)->make()->fill([
            'ethnicity' => $ethnicity,
        ]);

        $this->assertInstanceOf(Ethnicity::class, $biograph->ethnicity);
    }

    public function testGenderCanBeSetFromModel()
    {
        $gender = factory(Gender::class)->make();

        $biograph = factory(Biograph::class)->make()->fill([
            'gender' => $gender,
        ]);

        $this->assertInstanceOf(Gender::class, $biograph->gender);
    }

    public function testMaritalStatusCanBeSetFromModel()
    {
        $marital = factory(Marital::class)->make();

        $biograph = factory(Biograph::class)->make()->fill([
            'marital' => $marital,
        ]);

        $this->assertInstanceOf(Marital::class, $biograph->marital);
    }

    /**
     * Ensure Biograph cannot be soft deleted unless Personable has already been soft deleted.
     *
     * @return void
     */
    public function testCannotBeDeletedIfPersonableIsNotSoftDeleted()
    {
        $this->markTestSkipped('TODO: Test biograph cannot be soft deleted if Personable is not soft deleted');
    }
}
