<?php

namespace Tests\Feature\Listeners\Models\Users;

use Tests\TestCase;
use Myrtle\Users\User;
use Myrtle\People\Name;
use Myrtle\People\Biograph;
use Illuminate\Support\Facades\Queue;
use App\Listeners\Models\User\CascadeDeletes;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CascadeDeletesTest extends TestCase
{
    use RefreshDatabase;

    public function test_is_queued()
    {
        $this->markTestSkipped('Why is this not being pushed onto the queue?');
        Queue::fake();

        Queue::before(function () {
            dd('before');
        });

        factory(User::class)->create()->delete();

        Queue::assertPushed(CascadeDeletes::class);
    }

    public function test_relationships_soft_deleted()
    {
        factory(User::class)->create()->delete();

        $this->assertCount(0, Name::all());
        $this->assertCount(0, Biograph::all());
        $this->assertCount(1, Name::withTrashed()->get());
        $this->assertCount(1, Biograph::withTrashed()->get());
    }
}
