<?php

namespace Tests\Feature\Users;

use Tests\TestCase;
use Myrtle\Users\User;
use Myrtle\Users\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_returns_custom_collection()
    {
        $users = factory(User::class)->times(2)->create();

        $this->assertInstanceOf(Users::class, $users);
    }
}
