<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Saved;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SavedTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        Event::assertDispatched(Saved::class, function ($event) {
            return $event->user->id === $this->user->id;
        });
    }
}
