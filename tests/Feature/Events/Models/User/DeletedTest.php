<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Deleted;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeletedTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        $this->user->delete();

        Event::assertDispatched(Deleted::class, function ($event) {
            return $event->user->id === $this->user->id;
        });
    }
}
