<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Updating;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdatingTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        $this->user->update(['password' => 'testing']);

        Event::assertDispatched(Updating::class, function ($event) {
            return $event->user->password === $this->user->password;
        });
    }
}
