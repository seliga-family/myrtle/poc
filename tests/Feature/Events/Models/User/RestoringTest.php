<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Restoring;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RestoringTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        $this->markTestSkipped('Need to look into restoring');
        $this->user->delete();
        $this->user->restore();

        Event::assertDispatched(Restoring::class, function ($event) {
            return $event->user->id === $this->user->id;
        });
    }
}
