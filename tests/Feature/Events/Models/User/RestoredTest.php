<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Restored;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RestoredTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        $this->user->delete();
        $this->user->restore();

        Event::assertDispatched(Restored::class, function ($event) {
            return $event->user->id === $this->user->id;
        });
    }
}
