<?php

namespace Tests\Feature\Events\Models\User;

use Tests\TestCase;
use Myrtle\Users\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class BaseTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        Event::fake();

        $this->user = factory(User::class)->create();
    }
}
