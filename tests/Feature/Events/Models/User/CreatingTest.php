<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Creating;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatingTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        Event::assertDispatched(Creating::class, function ($event) {
            return $event->user->id === $this->user->id;
        });
    }
}
