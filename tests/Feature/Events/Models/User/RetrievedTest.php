<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Retrieved;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RetrievedTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        $this->markTestSkipped('Need to look into retrieved');

        $user = $this->user->fresh();

        Event::assertDispatched(Retrieved::class, function ($event) use ($user) {
            return $event->user->id === $user->id;
        });
    }
}
