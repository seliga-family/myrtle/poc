<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Deleting;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeletingTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        $this->user->delete();

        Event::assertDispatched(Deleting::class, function ($event) {
            return $event->user->id === $this->user->id;
        });
    }
}
