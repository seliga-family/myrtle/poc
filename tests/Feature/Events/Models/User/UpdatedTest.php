<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Updated;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdatedTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        $this->user->update(['password' => 'testing']);

        Event::assertDispatched(Updated::class, function ($event) {
            return $event->user->password === $this->user->password;
        });
    }
}
