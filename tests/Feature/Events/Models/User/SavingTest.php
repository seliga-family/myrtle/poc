<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Saving;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SavingTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        Event::assertDispatched(Saving::class, function ($event) {
            return $event->user->id === $this->user->id;
        });
    }
}
