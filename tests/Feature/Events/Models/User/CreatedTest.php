<?php

namespace Tests\Feature\Events\Models\User;

use Myrtle\Users\Events\Created;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatedTest extends BaseTest
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDispatched()
    {
        Event::assertDispatched(Created::class, function ($event) {
            return $event->user->id === $this->user->id;
        });
    }
}
