<?php

namespace Tests\Unit\Listeners\Models\Users;

use Tests\TestCase;
use Myrtle\Users\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CascadeDeletesTest extends TestCase
{
    use RefreshDatabase;

    public function test_listener_exists_if_user_restored_before_execution()
    {
        $this->markTestSkipped('How can this be tested?');
        $user = factory(User::class)->create();
        $user->delete();
    }
}
