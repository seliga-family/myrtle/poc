<?php

namespace Tests\Unit\Users\Jobs;

use Tests\TestCase;
use Myrtle\Users\User;
use Myrtle\Users\Jobs\UpdateName;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateNameTest extends TestCase
{
    use RefreshDatabase;

    protected $name = [
        'first' => 'Justin',
        'middle' => 'Ross',
        'last' => 'Seliga',
        'preferred' => 'agiles',
    ];

    public function test_job()
    {
        $user = factory(User::class)->create();

        UpdateName::dispatchNow($user, $this->name);

        tap($user->fresh(), function ($user) {
            collect($this->name)->each(function ($name, $part) use ($user) {
                $this->assertEquals($name, $user->name->$part);
            });
        });
    }
}
