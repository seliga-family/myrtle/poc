<?php

namespace Tests\Unit\Users\Jobs;

use Tests\TestCase;
use Myrtle\Users\Jobs\CreateUser;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateUserTest extends TestCase
{
    use RefreshDatabase;

    protected $name = [
        'first' => 'Justin',
        'middle' => 'Ross',
        'last' => 'Seliga',
        'preferred' => 'agiles',
    ];

    protected $emails = ['test@test.com'];

    protected $password = 'secret';

    public function test_job()
    {
        $user = CreateUser::dispatchNow(bcrypt($this->password), $this->name, $this->emails);

        tap($user->fresh(), function ($user) {
            $this->assertSame($this->emails, $user->emails->pluck('address')->toArray());
            collect($this->name)->each(function ($name, $part) use ($user) {
                $this->assertEquals($name, $user->name->$part);
            });
        });
    }
}
