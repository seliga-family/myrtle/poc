<?php

namespace Tests\Unit\Users\Jobs;

use Tests\TestCase;
use Myrtle\Users\User;
use Myrtle\Users\Jobs\CreateEmail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateEmailTest extends TestCase
{
    use RefreshDatabase;

    public function test_job()
    {
        $user = factory(User::class)->create();

        CreateEmail::dispatchNow($user, 'test@test.com');

        tap($user->fresh(), function ($user) {
            $this->assertCount(1, $user->emails);
            $this->assertEquals('test@test.com', $user->emails->first()->address);
        });
    }
}
