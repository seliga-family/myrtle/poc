@extends('layouts.app')

@section('body')
    <div class="min-h-screen flex flex-col md:flex-row items-center justify-center">
        <contacts></contacts>
    </div>
@endsection