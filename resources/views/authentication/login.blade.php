@extends('layouts.app')

@section('body')
<div class="flex h-screen">
    <div class="w-1/2 flex bg-grey-lightest items-center justify-center">
        <div class="flex flex-col w-3/4 md:w-3/5 items-center justify-center">

            <img class="w-2/3 mb-6 text-center" src="http://dream-well.test/images/ces/canvaslogo.svg" alt="">

            <div class="w-full flex flex-col mb-6">
                <div class="mt-6 bg-blue-lightest border-t-4 border-blue-light rounded-b text-blue-darkest px-4 py-3 shadow-md" role="alert">
                    <div class="flex justify-center">
                        <!-- <div class="py-1"><svg class="fill-current h-6 w-6 text-blue-darker mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div> -->
                        <div>
                            <p class="font-bold text-blue-darker">Google Chrome or Microsoft Edge work best</p>
                            <p class="text-sm">
                                <a class="text-blue no-underline" href="http://btod.pfizer.com/solution/150223104804940">Click to install Chrome on your Pfizer Windows PC.</a>
                            </p>
                        </div>
                    </div>
                </div>

                <input class="mt-6 py-3 px-2 text-grey-dark focus:border-grey-lighter border-2 border-transparent focus:outline-none" type="text" name="something" placeholder="me@here.com">
                <input class="py-3 px-2 mt-1 mb-4 text-grey-dark focus:border-grey-lighter border-2 border-transparent focus:outline-none" type="password" name="else" placeholder="secretsauce">
                <button class="py-3 px-2 mb-2 bg-blue text-grey-lightest hover:bg-blue-dark focus:bg-blue-dark hover:shadow-md focus:shadow-md w-100 font-bold text-lg">Log in</button>
                <div class="flex justify-between">
                    <div>
                        <input type="checkbox">
                        <span class="text-grey-dark">Remember email</span>
                    </div>
                    <a class="text-blue no-underline" href="#">Forgot Password?</a>
                </div>
            </div>



            <div class="mt-6 flex items-center justify-center">
                <img class="w-16 pr-2" src="https://canvas.pfizer.com/images/ces/pfizer_logo_blue.png" alt="">
                <span class="block text-grey-dark">Copyright © Pfizer Inc. 2018</span>
            </div>
        </div>
    </div>
    <div class="w-1/2 flex items-center justify-center">
        <div class="w-3/5">

        </div>
    </div>
</div>

@endsection