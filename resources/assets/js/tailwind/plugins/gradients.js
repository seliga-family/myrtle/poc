const _ = require('lodash')
let colors;

module.exports = function ({ increment = 45, filter = name => !name.includes('-') }) {
    return function ({ config, addUtilities }) {
        colors = config('colors');

        addUtilities(utilities(names(filter), increment));
    }
}

function utilities(names, increment) {
    return _.flatMap(names, first => {
        return _.flatMap(_.filter(names, second => second !== first), second => {
            return _.reduce(degrees(increment), (carry, degree) => {
                return _.concat(carry, css(first, second, degree));
            }, []);
        });
    });
}

function names(filter) {
    return _.filter(_.keys(colors), filter);
}

function degrees(increment) {
    let iterations = Math.floor(135 / increment);
    return _.reduce(Array.from({ length: iterations }), carry => {
        return _.concat(carry, _.last(carry) + increment);
    }, [0]);
}

function css(first, second, degree) {
    return {
        [`.bg-linear-${first}-${second}-${degree}`]: {
            background: `linear-gradient(${degree}deg, ${colors[first]}, ${colors[second]})`,
        }
    }
}