const _ = require('lodash')

module.exports = function ({ sizes = {
    0: '0',
    2: '2px',
    4: '4px',
    8: '8px',
    16: '16px'
}, variants = ['hover'] }) {
    return function ({ e, addUtilities }) {
        addUtilities([
            ..._.map(sizes, (value, size) => ({
                [`.${e(`blur-${size}`)}`]: { filter: `blur(${value})` },
            }))
        ], variants)
    }
}