let mix = require('laravel-mix');
let tailwind = require('tailwindcss');
require('laravel-mix-purgecss');
let purge = require('purgecss-webpack-plugin');
let glob = require('glob-all');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .purgeCss()
    .postCss('resources/assets/css/app.css', 'public/css')
    .options({
        postCss: [
            require('postcss-import')(),
            tailwind('./tailwind.js'),
            require('postcss-nesting')(),
        ]
    }).version();