<?php

namespace App\Http\Controllers\Genders;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class Seed extends Controller
{
    public function __invoke()
    {
        Artisan::call('db:seed', ['--class' => \GendersSeeder::class]);
    }
}
