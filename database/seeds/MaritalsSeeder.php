<?php

use Illuminate\Database\Seeder;
use Myrtle\People\Demographics\Marital;

class MaritalsSeeder extends Seeder
{
    public static $names = [
        'Single',
        'Married',
        'Divorced',
        'Widowed',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(static::$names)->each(function ($name) {
            Marital::updateOrCreate(['name' => $name]);
        });
    }
}
