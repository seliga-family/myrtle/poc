<?php

use Illuminate\Database\Seeder;
use Myrtle\People\Demographics\Gender;

class GendersSeeder extends Seeder
{
    public static $names = [
        'Male',
        'Female',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(static::$names)->each(function ($name) {
            Gender::updateOrCreate(['name' => $name]);
        });
    }
}
