<?php

use Faker\Generator as Faker;
use Myrtle\People\Demographics\Gender;

$factory->define(Gender::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
