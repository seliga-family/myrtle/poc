<?php

use Faker\Generator as Faker;
use Myrtle\People\Demographics\Ethnicity;

$factory->define(Ethnicity::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
