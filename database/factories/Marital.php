<?php

use Faker\Generator as Faker;
use Myrtle\People\Demographics\Marital;

$factory->define(Marital::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
