<?php

use Myrtle\People\Biograph;
use Faker\Generator as Faker;
use Myrtle\People\Demographics\Gender;
use Myrtle\People\Demographics\Marital;
use Myrtle\People\Demographics\Religion;
use Myrtle\People\Demographics\Ethnicity;

$factory->define(Biograph::class, function (Faker $faker) {
    return [
        'birth_date' => $faker->dateTime(),
        'gender_id' => factory(Gender::class)->create()->id,
        'religion_id' => factory(Religion::class)->create()->id,
        'ethnicity_id' => factory(Ethnicity::class)->create()->id,
        'marital_id' => factory(Marital::class)->create()->id,
    ];
});
