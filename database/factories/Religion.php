<?php

use Faker\Generator as Faker;
use Myrtle\People\Demographics\Religion;

$factory->define(Religion::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
