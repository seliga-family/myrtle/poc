<?php

use Myrtle\People\Name;
use Faker\Generator as Faker;

$factory->define(Name::class, function (Faker $faker) {
    $gender = $faker->boolean();

    return [
        'first' => $gender ? $faker->firstNameMale : $faker->firstNameFemale,
        'middle' => $gender ? $faker->firstNameMale : $faker->firstNameFemale,
        'last' => $faker->lastName,
    ];
});
