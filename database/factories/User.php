<?php

use Myrtle\Users\User;
use Myrtle\People\Name;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    static $password;

    return [
        'password' => $password ?: $password = bcrypt('secret'),
        'timezone' => $faker->timezone,
        'remember_token' => str_random(10),
    ];
});

$factory->state(User::class, 'with:name', function () {
    return [];
})->afterCreatingState(User::class, 'with:name', function ($user) {
    $user->name()->update(factory(Name::class)->make()->toArray());
});

$factory->state(User::class, 'with:name', function () {
    return [
        'name_id' => function () {
            return factory(Name::class)->create()->id;
        },
    ];
});
