<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $timezone
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string|null $disabled_at
 * @property string|null $last_visit_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDisabledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastVisitAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedBy($value)
 */
	class User extends \Eloquent {}
}

namespace Myrtle\Emails{
/**
 * Myrtle\Emails\Email
 *
 * @property int $id
 * @property int $emailable_id
 * @property string $emailable_type
 * @property string $address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Myrtle\Emails\Email[] $emailable
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email whereEmailableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email whereEmailableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Emails\Email whereUpdatedAt($value)
 */
	class Email extends \Eloquent {}
}

namespace Myrtle\People{
/**
 * Myrtle\People\Name
 *
 * @property int $id
 * @property int $personable_id
 * @property string $personable_type
 * @property string|null $title
 * @property string|null $first
 * @property string|null $middle
 * @property string|null $last
 * @property string|null $suffix
 * @property void $preferred
 * @property array|null $monikers
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read string $first_initial
 * @property-read string $first_last
 * @property-read string $first_last_initials
 * @property-read string $full
 * @property-read string $initials
 * @property-read string $last_first
 * @property-read string $last_first_initials
 * @property-read string $last_initial
 * @property-read string $middle_initial
 * @property-read string $preferred_last
 * @property-read \Illuminate\Database\Eloquent\Collection|\Myrtle\People\Name[] $personable
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\People\Name onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereFirst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereLast($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereMiddle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereMonikers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name wherePersonableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name wherePersonableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name wherePreferred($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereSuffix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Name whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\People\Name withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\People\Name withoutTrashed()
 */
	class Name extends \Eloquent {}
}

namespace Myrtle\People{
/**
 * Myrtle\People\Biograph
 *
 * @property int $id
 * @property int $personable_id
 * @property string $personable_type
 * @property \Illuminate\Support\Carbon|null $birth_date
 * @property int|null $gender_id
 * @property int|null $ethnicity_id
 * @property int|null $marital_status_id
 * @property int|null $religion_id
 * @property \Illuminate\Support\Carbon|null $death_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Myrtle\People\Demographics\Ethnicity|null $ethnicity
 * @property \Myrtle\People\Demographics\Gender|null $gender
 * @property \Myrtle\People\Demographics\Marital $marital
 * @property-read \Illuminate\Database\Eloquent\Collection|\Myrtle\People\Biograph[] $personable
 * @property \Myrtle\People\Demographics\Religion|null $religion
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\People\Biograph onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereDeathDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereEthnicityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereGenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereMaritalStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph wherePersonableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph wherePersonableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereReligionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Biograph whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\People\Biograph withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\People\Biograph withoutTrashed()
 */
	class Biograph extends \Eloquent {}
}

namespace Myrtle\People\Demographics{
/**
 * Myrtle\People\Demographics\Religion
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Religion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Religion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Religion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Religion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Religion whereName($value)
 */
	class Religion extends \Eloquent {}
}

namespace Myrtle\People\Demographics{
/**
 * Myrtle\People\Demographics\Gender
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Gender newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Gender newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Gender query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Gender whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Gender whereName($value)
 */
	class Gender extends \Eloquent {}
}

namespace Myrtle\People\Demographics{
/**
 * Myrtle\People\Demographics\Ethnicity
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Ethnicity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Ethnicity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Ethnicity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Ethnicity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Ethnicity whereName($value)
 */
	class Ethnicity extends \Eloquent {}
}

namespace Myrtle\People\Demographics{
/**
 * Myrtle\People\Demographics\Marital
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Marital newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Marital newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\People\Demographics\Marital query()
 */
	class Marital extends \Eloquent {}
}

namespace Myrtle\Users{
/**
 * Myrtle\Users\User
 *
 * @property int $id
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed $timezone
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $disabled_at
 * @property string|null $last_visit_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Myrtle\People\Biograph $biograph
 * @property-read \Illuminate\Database\Eloquent\Collection|\Myrtle\Emails\Email[] $emails
 * @property \Myrtle\People\Demographics\Ethnicity $ethnicity
 * @property \Myrtle\People\Demographics\Gender $gender
 * @property \Myrtle\People\Demographics\Marital $marital
 * @property-read \Myrtle\People\Name $name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property \Myrtle\People\Demographics\Religion $religion
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User byEthnicity(\Myrtle\People\Demographics\Ethnicity $ethnicity)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User byGender(\Myrtle\People\Demographics\Gender $gender)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User byMarital(\Myrtle\People\Demographics\Marital $marital)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User byReligion(\Myrtle\People\Demographics\Religion $religion)
 * @method static bool|null forceDelete()
 * @method static \Myrtle\Users\Builders\User|\Myrtle\Users\User newModelQuery()
 * @method static \Myrtle\Users\Builders\User|\Myrtle\Users\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\Users\User onlyTrashed()
 * @method static \Myrtle\Users\Builders\User|\Myrtle\Users\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereDisabledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereLastVisitAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Myrtle\Users\User whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\Users\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Myrtle\Users\User withoutTrashed()
 */
	class User extends \Eloquent {}
}

